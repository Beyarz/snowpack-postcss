/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
	mount: {},
	root: './src/',
	plugins: [
		'@snowpack/plugin-postcss',
		'@snowpack/plugin-sass'
	],
	packageOptions: {},
	devOptions: {},
	buildOptions: {},
};
